/*
                         使用说明
在值标签不存在不可转换字符的前提下，将多个dta文件由gb18030转换至uft-8编码。
可能出现乱码的地方有：数据标签、变量名、变量内容、变量标签、值标签
其他的注意事项如下：
1. 在当前目录下生成了“backup files”子文件夹。
2. 将原dta数据保存至了“backup files”子文件夹中。
3. 转码后utf8格式的dta数据，覆盖了当前文件夹的原数据。
4. 此程序只能转一遍，只能转一遍，只能转一遍！否则原数据将丢失，且数据重新变回乱码。
5. 该程序会将当前文件夹的所有dta文件进行上述操作。
6. 若值标签存在不可转换的字符，则需手动更新正确并保存后再进行50行以及之后的操作。
7. 该程序要求原dta文件为gb2312、gbk或gb18030编码，否则可能转码不成功。
*/

*------------------------用户修改部分--------------------------------
// cd "D:\stata15\ado\personal\Net_course"


*-------------------------无需变更部分--------------------------------
local dir_dta: dir . files "*.dta", respectcase //windows系统需要加respectcase选项以区分大小写

cap mkdir "backup files"  //建立备份文件夹
foreach dta_file of local dir_dta {
	qui copy `dta_file' "backup files\", replace //将当前文件夹的dta数据备份至"backup files"文件夹中
	qui use `dta_file', clear
	
	*对数据标签进行转码
	local data_lbl: data label
	local data_lbl = ustrfrom("`data_lbl'", "gb18030", 1)
	label data "`data_lbl'"
	
	*对变量名、变量标签、字符型变量取值转码
	foreach v of varlist _all {
			* 对字符型变量取值进行转码
			local type: type `v'   //将变量的类型放入宏type中
			if strpos("`type'", "str") {
				replace `v' = ustrfrom(`v', "gb18030", 1)   //如果变量是字符型变量，使用ustrfrom()函数进行转码
			}
		* 对变量标签进行转码
		local lbl: var label `v'   //将变量的标签放入宏lbl中
		local lbl = ustrfrom("`lbl'", "gb18030", 1)   //使用ustrfrom()函数对`lbl'转码
		label var `v' `"`lbl'"'   //将转码后的字符串作为变量的标签
		* 对变量名进行转码
		local newname = ustrfrom(`"`v'"', "gb18030", 1)   //使用ustrfrom()函数将变量名字符串进行转码
		qui rename `v' `newname'   //将转码后的字符串重命名为变量名
	}

	*下面对数值标签进行转码
	qui label save using label.do, replace   //将数值标签的程序保存到label.do中
	preserve
	*- 将do文件的内容用txt的方式导入一个变量之中，然后再对该变量进行拉直
	qui import delimited using label.do, ///
	  varnames(nonames) delimiters("asf:d5f15g4dsf9qw8d4d5d",asstring) encoding(gb18030) clear
	qui describe
	if r(N) == 0 {
		restore
		save `dta_file', replace
	}
	else {
		qui levelsof v1, local(v1_lev)
		restore
		foreach label_modify of local v1_lev {    //这个的foreach与前面的local(v_lev)对应，细节可查看帮助文件
		`label_modify' //依次对原数据执行值标签替换操作
		} 
		save `dta_file', replace  //将转码好的dta文件替换原来的dta文件
	}
		
	* 删除中间临时文件
	erase label.do
}

